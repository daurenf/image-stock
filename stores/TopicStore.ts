import { getTopics } from "~/services/topics";

export const useTopicStore = defineStore("topicStore", () => {
  const topics = ref<Array<Topic>>([]);

  async function setTopics() {
    const newTopics = await getTopics();


    topics.value = newTopics;
  }

  return { topics, setTopics };
});
