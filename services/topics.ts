import { BASE_URL, API_KEY } from "./unsplash.config";

export const getTopics = async () => {
  const headers = {
    Authorization: `Client-ID ${API_KEY}`,
  };

  const { data: topics } = await useFetch<Array<Topic>>(`${BASE_URL}/topics`, {
    headers,
  });

  return topics.value ?? [];
};
