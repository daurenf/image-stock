import { BASE_URL, API_KEY } from "./unsplash.config";

const headers = {
  Authorization: `Client-ID ${API_KEY}`,
};

export const getPhotos = async ({
  page,
  topic,
  term,
}: {
  page: number;
  topic?: string;
  term?: string;
}) => {
  let url = BASE_URL;

  const queryParams: { page: number; query?: string } = {
    page,
  };

  if (term) {
    url += `/search/photos`;
    queryParams["query"] = term;
  } else if (topic) {
    url += `/topics/${topic}/photos`;
  } else {
    url += `/photos`;
  }

  try {
    const { data: newImages } = await useAsyncData<Array<Image>>(
      url,
      () => $fetch(url, { headers, query: queryParams }),
      {
        transform: (response: any) => {
          if (response.results === null || response.results === undefined) {
            return response;
          } else {
            return response.results;
          }
        },
      }
    );

    return newImages.value || [];
  } catch (err: any) {
    throw new Error(err.message);
  }
};

export const getFavoritePhotos = async () => {
  const favorites = JSON.parse(localStorage.getItem("favorites") || "[]");


  const favoriteImages = await Promise.all(
    favorites.map(async (id: string) => {
      const favImg = await $fetch<Image>(`${BASE_URL}/photos/${id}`, {
        headers,
      });

      return { ...favImg, favorite: true };
    })
  );

  return favoriteImages;
};
