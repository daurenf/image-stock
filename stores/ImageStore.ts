import { getPhotos, getFavoritePhotos } from "~/services/photos";

export const useImageStore = defineStore("imageStore", () => {
  const pending = ref<boolean>(false);
  const currentPage = ref<number>(1);
  const currentTopic = ref<string>("");
  const searchTerm = ref<string>("");

  const images = ref<Array<Image>>([]);
  async function setImages(payload: { topic?: string; term?: string }) {
    currentPage.value = 1;

    const { topic, term } = payload;

    if (topic) {
      currentTopic.value = topic;
    }

    if (term) {
      searchTerm.value = term;
    }

    const newImages = await getPhotos({
      page: currentPage.value,
      topic: currentTopic.value,
      term: searchTerm.value,
    });

    const favorites = JSON.parse(localStorage.getItem("favorites") || "[]");

    newImages.forEach(img => {
      if (favorites.includes(img.id)) img.favorite = true;
    });

    images.value = newImages;
  }

  async function setFavoriteImages() {
    currentPage.value = 1;

    const favoriteImages = await getFavoritePhotos();

    images.value = favoriteImages;
  }

  async function loadImages() {
    currentPage.value++;

    const newImages = await getPhotos({
      page: currentPage.value,
      topic: currentTopic.value,
      term: searchTerm.value,
    });

    const favorites = JSON.parse(localStorage.getItem("favorites") || "[]");

    newImages.forEach(img => {
      if (favorites.includes(img.id)) img.favorite = true;
    });

    images.value.push(...newImages);
  }

  function resetAll() {
    images.value = [];
    currentPage.value = 0;
    currentTopic.value = searchTerm.value = "";
  }

  return {
    images,
    setImages,
    loadImages,
    setFavoriteImages,
    resetAll,
    currentTopic,
    pending,
  };
});
