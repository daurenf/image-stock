import { nanoid } from "nanoid";

export const useSearchHistoryStore = defineStore("searchHistoryStore", () => {
  const searchHistory = ref<Array<SearchTerm>>([]);

  function setSearchHistory() {
    const history = JSON.parse(localStorage.getItem("search-history") || "[]");
    searchHistory.value = history;
  }

  function addSearchTermToHistory(searchTerm: { term: string; date: number }) {
    searchHistory.value.push({ id: nanoid(), ...searchTerm });
    const history = JSON.parse(localStorage.getItem("search-history") ?? "[]");
    history.push(searchTerm);
    localStorage.setItem("search-history", JSON.stringify(history));
  }

  return { searchHistory, setSearchHistory, addSearchTermToHistory };
});
