export {};

declare global {
  interface Image {
    id: string;
    urls: {
      regular: string;
    };
    description: string;
    user: {
      first_name: string;
      last_name: string | null;
      username: string;
      profile_image: {
        small: string;
      };
    };
    links: {
      download: string;
    };
    favorite?: boolean;
  }

  interface Topic {
    id: string;
    title: string;
  }

  interface SearchTerm {
    id: string;
    term: string;
    date: number;
  }
}
