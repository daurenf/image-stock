export const useSettingsStore = defineStore("settingsStore", () => {
  const selectedView = ref<string>("");

  function setViewMode() {
    const viewMode = localStorage.getItem("view_mode") ?? "grid";
    selectedView.value = viewMode;
  }

  function changeViewMode(viewMode: string) {
    selectedView.value = viewMode;
    localStorage.setItem("view_mode", viewMode);
  }

  return { selectedView, setViewMode, changeViewMode };
});
